<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Product
{
   
    private $id;

    private $brochure;

    public function getId()
    {
        return $this->id;
    }

    public function getBrochure()
    {
        return $this->brochure;
    }

    public function setBrochure($brochure)
    {
        $this->brochure = $brochure;

        return $this;
    }
}
